#define CONIX_STDDEF
#define NULL 0

/*
 * Type for storing the number of elements in arrays and etc.
 */
typedef long size_t;
