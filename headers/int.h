#define CONIX_INT

/*
 * The function getLengthInt() gets an integer variable (int n) 
 * and returns its length.
 */
int getLengthInt(int n);
/*
 * The function gets the integer variable (int n) and (int member),
 * and returns a number from the number n by the number specified in member.
 */
int getMemberInt(int n, int member);
/*
 * The powInt() function returns a number (int n)
 * raised to the power (int n).
 */
int powInt(int n, int p);
