#define CONIX_STDLIB

/*
 * Puts the elements in a row in reverse order
 */
void reverse(char *str);
/*
 * Converts integer variables to a string
 */
void  itoa(int n, char *s);
